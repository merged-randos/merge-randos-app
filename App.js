import React from 'react';
import { StatusBar } from 'react-native';
import Home from './src/Home';

export default function () {
  return (
    <>
      <StatusBar barStyle='dark-content' />
      <Home />
    </>
  );
}