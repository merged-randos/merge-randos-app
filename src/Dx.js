import * as MediaLibrary from 'expo-media-library'
import React, { useState, useRef, useEffect } from 'react'
import { Button, View, Image, ImageBackground, StyleSheet, Text, Pressable} from 'react-native'
import { captureRef } from "react-native-view-shot"
import Loading from './Loading'

export default function Dx() {
	const [rollImages, setRollImages] = useState(null) // image state
	const [saved, setSaved] = useState(false) // save button state
	const viewShotRef =  useRef() // ref used for view shot tool

	// get initial images
	useEffect(() => { getImages() }, [])

	// generate random number within array range
	const rando = (array) => {
		return Math.floor(Math.random() * array.length)
	}

	// ask for camera roll permission, get two random images from camera roll,
	// put them in state, and set save button state to active
	const getImages = async () => {
		const { status } = await MediaLibrary.requestPermissionsAsync()
		const media = await MediaLibrary.getAssetsAsync({mediaType: 'photo', first: 5000})
		const index1 = rando(media.assets)
		const image1 = media.assets[index1].uri
		media.assets.splice(index1, 1) // remove first image from array to avoid redundancy 
		const index2 = rando(media.assets)
		const image2 = media.assets[index2].uri
		setRollImages({ bgImage: image1, topImage: image2 })
		setSaved(false)
	}

	// take view shot, save it to camera roll, and set save button state to inactive
	const saveImage = async () => {
		const captureImage = await captureRef(viewShotRef, { format: "jpg", quality: 0.8 })
		const save = MediaLibrary.saveToLibraryAsync(captureImage)
		setSaved(true)
	}

	const styles = StyleSheet.create({
		container: {
			flex: 1,
			alignItems: 'center',
			justifyContent: 'center',
			marginBottom: 50
		},
		imageSize: {
			width: 350,
			height: 350
		},
		opacity: {
			opacity: .5
		},
		button: {
			borderRadius: 10,
			width: 200,
			height: 50,
			marginTop: 30,
			alignItems: 'center',
			justifyContent: 'center'
		},
		buttonText: {
			fontSize: 20,
			fontWeight: '500'
		},
		nextText: {
			color: 'white'
		},
		savedText: {
			color: 'gray'
		}
		// additional button colors declared inline due to pressing needs
	});

	// save button with inactive and active states respectively
	const saveButton = saved
		? (
			<View style={styles.button}>
				<Text style={[
					styles.buttonText, 
					styles.savedText
				]}>
					Saved
				</Text>
			</View>
		) 
		: (
			<Pressable
				onPress={saveImage}
				style={styles.button}>
				{({ pressed }) => (
					<Text style={[
						{ color: pressed ? '#006b2d' : '#00b74d' }, 
						styles.buttonText
					]}>
						Save to Photos
					</Text>
				)}
			</Pressable>
		)

	// render loading spinner if there's no images in state
	if (!rollImages) {
		return (
			<Loading />
		)
	}

	// render thee app
	return (
		<View style={styles.container}>
			<View ref={viewShotRef}>
				<ImageBackground source={{ uri: rollImages.bgImage }} style={styles.imageSize}>
					<Image
						source={{ uri: rollImages.topImage }}
						style={[styles.opacity, styles.imageSize]}
						resizeMode='cover'
					/>
				</ImageBackground>
			</View>
			<View>
				<Pressable
					onPress={getImages}
					style={({ pressed }) => [
						{ backgroundColor: pressed ? '#b700a7' : '#d800ea' }, 
						styles.button
					]
				}>
					<Text style={[
						styles.nextText, 
						styles.buttonText
					]}>
						New
					</Text>
				</Pressable>
			</View>
			{saveButton}
		</View>
	)
}