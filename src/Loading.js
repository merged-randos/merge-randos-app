import React from 'react'
import { View, ActivityIndicator, StyleSheet } from 'react-native'

export default function Loading() {  
	const styles = StyleSheet.create({
		container: {
			flex: 1,
			justifyContent: "center",
			justifyContent: "space-around",
			padding: 10,
			marginBottom: 50
		}
	});

	return (
		<View style={styles.container}>
			<ActivityIndicator size="large" />
		</View>
	)
}