import React, { Fragment } from 'react';
import { Text, View, Linking } from 'react-native'

export default function About() {
	return (
		<Fragment>
			<Text style={{marginHorizontal: 20, marginTop: 20, fontSize: 30}}>
				Hey. Hope you're enjoying the app. Check out the
				<Text
					style={{color: '#00b74d'}}
					onPress={() => {Linking.openURL('http://mergedrandos.com')}}
				>
					{` site `}
				</Text>
				and 
				<Text
					style={{color: '#00b74d'}}
					onPress={() => {Linking.openURL('https://twitter.com/mergedrandos')}}
				>
					{` twitter `}
				</Text>
				for more sick double exposures.
			</Text>
		</Fragment>
	)
}