import React, { useState, Fragment } from 'react';
import { View, Text, Image, Pressable } from 'react-native'
import Dx from './Dx'
import About from './About'

export default function Home() {
	const [aboutActive, setAboutActive] = useState(false)

	const toggleActive = () => {
		aboutActive === true ? setAboutActive(false) : setAboutActive(true)
	}

	const logo = (
		<Pressable onPress={toggleActive} style={{marginTop: 50, marginLeft: 20, width: 50}}>
			<Image source={require('./assets/logo-40.png')} />
		</Pressable>
	)

	if (aboutActive) {
		return (
			<Fragment>
				{logo}
				<About />
				<Text
				    style={{color: '#00b74d', marginLeft: 20, marginTop: 20, fontSize: 30}}
				    onPress={toggleActive}
				  >
				    {`<-- back to app`}
				  </Text>
			</Fragment>
		)
	}
	return (
		<Fragment>
			{logo}
			<Dx />
		</Fragment>
	)
}



			